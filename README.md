# Learning Android #

* Code from O'Reilly's Learning Android 2nd Edition (Gargenta, Nakamura)
* v1.0


## Changes ##
* Minor changes here and there to get things working.
* Bug fixes.
* Client lib updated, code and jar available at: https://github.com/divukman/YambaClientLib/tree/master/bin