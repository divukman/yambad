package com.dimitar.android.yamba;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/***
 * This is our database helper class for managing creating and upgrading the database.
 * 
 * @author dvukman
 *
 */
public class DbHelper extends SQLiteOpenHelper {
	public static final String TAG = DbHelper.class.getSimpleName();

	
	public DbHelper(Context context) {
		super (context, StatusContract.DB_NAME, null, StatusContract.DB_VERSION);
	}
	
	
	/***
	 * Called only first time we create the database.
	 */
	@Override
	public void onCreate(SQLiteDatabase db) { 	
		final String sql = String.format("create table %s (%s int primary key, %s text, %s text, %s int)", 
									StatusContract.TABLE,
									StatusContract.Column.ID,
									StatusContract.Column.USER,
									StatusContract.Column.MESSAGE,
									StatusContract.Column.CREATED_AT);		
		Log.d(TAG, "onCreate with SQL: " + sql);
		db.execSQL(sql);
	}

	/***
	 * Gets called whenever existing version != new version, i.e. schema changed.
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Typically you do ALTER TABLE here...
		db.execSQL("drop table if exists " + StatusContract.TABLE);
		onCreate(db);
	}

}
