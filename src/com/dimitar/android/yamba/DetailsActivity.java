package com.dimitar.android.yamba;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class DetailsActivity extends AppCompatActivity {
	
	DetailsFragment mDetailsfragment = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {		
		super.onCreate(savedInstanceState);					
		
		if (savedInstanceState == null) {
			mDetailsfragment = new DetailsFragment();			
			getFragmentManager().beginTransaction().add(android.R.id.content, mDetailsfragment, mDetailsfragment.getClass().getSimpleName()).commit();			
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		Intent intent = getIntent();
		
		if (intent != null && mDetailsfragment != null) {//todo fix
			long id = intent.getLongExtra(StatusContract.Column.ID, -1);
			mDetailsfragment.updateView(id);
		}
		
	}
}
