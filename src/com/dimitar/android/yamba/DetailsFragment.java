package com.dimitar.android.yamba;

import android.app.Fragment;
import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class DetailsFragment extends Fragment {
	
	private TextView mTxtUser = null;
	private TextView mTxtMessage = null;
	private TextView mTxtCreatedAt = null;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.list_item, null, false);
		
		mTxtUser = (TextView) view.findViewById(R.id.list_item_text_user);
		mTxtMessage = (TextView) view.findViewById(R.id.list_item_text_message);
		mTxtCreatedAt = (TextView) view.findViewById(R.id.list_item_text_created_at);
		
		return view;
	}
	
	@Override
	public void onResume() {	
		super.onResume();
		long id = getActivity().getIntent().getLongExtra(StatusContract.Column.ID, -1);
		updateView(id);
	}

	public void updateView(long id) {
		if (id == -1) {
			mTxtCreatedAt.setText("");
			mTxtMessage.setText("");
			mTxtUser.setText("");
		} else {
			Uri uri = ContentUris.withAppendedId(StatusContract.CONTENT_URI, id);
			Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
			
			if (!cursor.moveToFirst()) {
				return; //todo refactor this
			}
			
			String user = cursor.getString(cursor.getColumnIndex(StatusContract.Column.USER));
			String message = cursor.getString(cursor.getColumnIndex(StatusContract.Column.MESSAGE));
			long createdAt = cursor.getLong(cursor.getColumnIndex(StatusContract.Column.CREATED_AT));
			
			mTxtUser.setText(user);
			mTxtMessage.setText(message);
			mTxtCreatedAt.setText(DateUtils.getRelativeTimeSpanString(createdAt));
		}
	}
	
	

}
