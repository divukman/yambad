package com.dimitar.android.yamba;

import java.util.List;

import com.marakana.android.yamba.clientlib.YambaClient;

import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.service.wallpaper.WallpaperService;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.widget.Toast;

public class YambaWallpaper extends WallpaperService {

	@Override
	public Engine onCreateEngine() {
		return new YambaWallpaperEngine();
	}
	
	private class YambaWallpaperEngine extends Engine implements Runnable {
		
		//@todo: introduce naming convention for member variables, ie m or m_ or _
		private Handler handler = new Handler();
		private ContentThread contentThread = new ContentThread();
		private boolean running = true;
		
		private YambaClient yambaclient = null;
		private Paint paint = null;
		
		private String[] content = new String[20];
		private TextPoint[] textPoints = new TextPoint[20];
		private int current = -1;
		private float offset = 0;
		
		
		public YambaWallpaperEngine() {
			
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
			
			final String username = prefs.getString("username", "");
			final String password = prefs.getString("password", "");
			
			if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
				Toast.makeText(getApplicationContext(), "Please update your username and password.", Toast.LENGTH_LONG).show();
				return;
			}
			
			this.yambaclient =  new YambaClient(username, password);
			
			paint = new Paint();
			paint.setColor(0xffffffff);
			paint.setAntiAlias(true);
			paint.setStrokeWidth(1);
			paint.setStyle(Paint.Style.FILL);
			paint.setStrokeCap(Paint.Cap.SQUARE);
			paint.setTextSize(40);
			
		}
		
		@Override
		public void onCreate(SurfaceHolder surfaceHolder) {		
			super.onCreate(surfaceHolder);
			
			running = true;
			contentThread.start();
			
			//enable touch events
			setTouchEventsEnabled(true);
		}
		
		@Override
		public void onDestroy() {		
			super.onDestroy();
			handler.removeCallbacks(this);
			
			running = false;
			
			synchronized (contentThread) {
				contentThread.interrupt();
			}
		}
		
		@Override
		public void onVisibilityChanged(boolean visible) {			
			if (visible) {
				drawFrame();
			} else {
				handler.removeCallbacks(this);
			}
		}
		
		@Override
		public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {		
			super.onSurfaceChanged(holder, format, width, height);
			drawFrame();
		}
		
		@Override
		public void onSurfaceCreated(SurfaceHolder holder) {		
			super.onSurfaceCreated(holder);			
		}
		
		@Override
		public void onSurfaceDestroyed(SurfaceHolder holder) {
			super.onSurfaceDestroyed(holder);
			handler.removeCallbacks(this);
		}
		
		@Override
		public void onOffsetsChanged(float xOffset, float yOffset, float xOffsetStep, float yOffsetStep,
				int xPixelOffset, int yPixelOffset) {
			offset = xPixelOffset;
			drawFrame();
		}
		
		
		@Override
		public void onTouchEvent(MotionEvent event) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				current++;
				
				if (current >= textPoints.length) {
					current = 0;
				}
				
				String text = content[current];
				if (text != null) {
					textPoints[current] = new TextPoint(text, event.getX() - offset, event.getY());
				}
			}
			
			super.onTouchEvent(event);
		}
		
		private void drawFrame() {
			final SurfaceHolder holder = getSurfaceHolder();
			Canvas c = null;
			
			try {
				c = holder.lockCanvas();
				if (c!= null) {
					//draw text
					drawText(c);
				}
			} catch(Exception e) {
				e.printStackTrace();
				//@todo: log exception
			} finally {
				if (c != null) {
					holder.unlockCanvasAndPost(c);
				}
			}
			
			//Reschedule the next redraw
			handler.removeCallbacks(this);
			
			if (isVisible()) {
				handler.postDelayed(this, 40); //40ms = 25 frames per second
			}
		}
		
		private void drawText(Canvas c) {
			c.drawColor(Color.BLACK);
			
			for (TextPoint textpoint: textPoints) {
				if (textpoint != null) {
					c.drawText(textpoint.text, textpoint.x + offset, textpoint.y + offset, paint);
				}
			}
		}
		
		private class ContentThread extends Thread {
			public void run() {
				while(running) {
					try {
						boolean hascontent = getContent();
						if (hascontent) {
							Thread.sleep(60 * 1000);
						} else {
							Thread.sleep(2 * 1000);
						}
					} catch (InterruptedException ie) {
						//@todo log
						return;
					} catch(Exception e) {
						//@todo log
					}
				}
			}
		}
		
		private class TextPoint {
			public String text;
			public float x;
			public float y;
			
			public TextPoint(String t, float xp, float yp) {
				this.text = t;
				this.x = xp;
				this.y = yp;
			}
		}
		
		
		private boolean getContent() {
			List <YambaClient.Status> timeline = null;
			
			try {
				timeline = yambaclient.getTimeline(20);
				
				int i = -1;
				content = new String[20];
				if (timeline != null) {
					for (YambaClient.Status status: timeline) {
						i++;
						content[i] = status.getMessage();
					}
				}				
			} catch(Exception e) {
				//@todo log
			}
			
			return timeline != null && !timeline.isEmpty();
		}
		

		@Override
		public void run() {
			drawFrame();
		}
		
	}

}
