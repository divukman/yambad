package com.dimitar.android.yamba;

import android.app.ListFragment;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.SimpleCursorAdapter.ViewBinder;
import android.widget.TextView;

public class TimelineFragment extends ListFragment implements LoaderCallbacks<Cursor> {
	private static final String TAG = TimelineFragment.class.getSimpleName();
	
	private static final String [] FROM = {
			StatusContract.Column.USER,
			StatusContract.Column.MESSAGE,
			StatusContract.Column.CREATED_AT
			};
	
	private static final int[] TO = {
		R.id.list_item_text_user,
		R.id.list_item_text_message,
		R.id.list_item_text_created_at
	};
	
	private SimpleCursorAdapter mAdapter = null;
	
	private static final int LOADER_ID = 42;
	
	
	@SuppressWarnings("deprecation")
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {		
		super.onActivityCreated(savedInstanceState);
		
		setEmptyText("Loading data...");
		
		mAdapter = new SimpleCursorAdapter(getActivity(), R.layout.list_item, null, FROM, TO);
		mAdapter.setViewBinder(new TimelineViewBinder());
		
		setListAdapter(mAdapter);
		getLoaderManager().initLoader(LOADER_ID, null, this);
	}


	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		Loader <Cursor> result = null;
		
		if (id == LOADER_ID) {		
			Log.d(TAG, "onCreateLoader");
			result = new CursorLoader(getActivity(), StatusContract.CONTENT_URI, null, null, null, StatusContract.DEFAULT_SORT);
		}
		
		return result;
	}

	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {		
		DetailsFragment fragment = (DetailsFragment) getFragmentManager().findFragmentById(R.id.fragment_details);
		if (fragment != null && fragment.isVisible()) {
			fragment.updateView(id);
		} else {
			startActivity(new Intent(getActivity(), DetailsActivity.class).putExtra(StatusContract.Column.ID, id));
		}
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		Log.d(TAG, "onFinishedLoad with cursor: " + data.getCount());
		mAdapter.swapCursor(data);
	}


	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		mAdapter.swapCursor(null);
	}
	
	
	class TimelineViewBinder implements ViewBinder {

		@Override
		public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
			boolean result = false;
			
			if (view.getId() == R.id.list_item_text_created_at) {
				
				//Convert long timestamp to relative time
				long timestamp = cursor.getLong(columnIndex);
				CharSequence relativeTime = DateUtils.getRelativeTimeSpanString(timestamp);
				((TextView) view).setText(relativeTime);
				
				result = true;
			}
			
			return result;
		}
		
	}
}
