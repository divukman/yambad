package com.dimitar.android.yamba;

import com.marakana.android.yamba.clientlib.YambaClient;
import com.marakana.android.yamba.clientlib.YambaClientException;

import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class StatusFragment extends Fragment implements OnClickListener {

	public static final String TAG = "StatusFragment";
	
	private Button mBtnTweet = null;
	private EditText mEditTxtStatus = null;
	private TextView mTxtViewCount = null;
	private int mDefaultColor = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstance) {
		
		View view = inflater.inflate(R.layout.fragment_status, container, false);
		
		mBtnTweet = (Button) view.findViewById(R.id.buttonTweet);
		mEditTxtStatus = (EditText) view.findViewById(R.id.editStatus);
		mTxtViewCount = (TextView) view.findViewById(R.id.txtViewCount);
		
		mBtnTweet.setOnClickListener(this);
		mDefaultColor = mTxtViewCount.getTextColors().getDefaultColor();
		
		mEditTxtStatus.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {				
				final int count = 140 - mEditTxtStatus.length();
				mTxtViewCount.setText(String.valueOf(count));
				
				
				if (count > 10) {
					mTxtViewCount.setTextColor(Color.GREEN);					
				} else {
					mTxtViewCount.setTextColor(Color.RED);
				}
			}
		});
		
		return view;
	}


	@Override
	public void onClick(View v) {
		final String status = mEditTxtStatus.getText().toString();
		Log.d(TAG, "onClicked with status: " + status);
		
		new PostTask().execute(status);		
	}
	
	
	private final class PostTask extends AsyncTask <String, Void, String> {

		@Override
		protected String doInBackground(String... params) {			
			String result = null;
			
			final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
			final String userName = prefs.getString("username", "");
			final String password = prefs.getString("password", "");
			
			Log.d(TAG, "Shared preferences: username = " + userName + ", pass: " + password);
			
			if (TextUtils.isEmpty(userName) || TextUtils.isEmpty(password)) {
				getActivity().startActivity(new Intent(getActivity(), SettingsActivity.class));
				return "Please update your username and password.";
			}
			
			final String status = params[0];			
			YambaClient yambaClient = new YambaClient("student", "password");
			try {
				yambaClient.postStatus(status);
				result = "Successfully posted!";
			} catch (YambaClientException ex) {
				ex.printStackTrace();
				result = "Failed posting to Yamba Service!";
			}
			
			return result;
		}
		
		@Override
		protected void onPostExecute(String result) {			
			super.onPostExecute(result);
			
			Toast.makeText(StatusFragment.this.getActivity(), result, Toast.LENGTH_LONG).show();
		}
		
	}
}
